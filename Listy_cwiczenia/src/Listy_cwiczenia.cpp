//============================================================================
// Name        : Listy_cwiczenia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
// 2 listy z jednej od 3 do 7 do drugiej listy
#include <iostream>
#include <list>
using namespace std;

bool parzyste(const int& value) {
	return (value%2==0);

}

bool compare( const int&first, const int& second){

}
int main() {
	list<int> mylist1;
	list<int> mylist2;
	for (int i = 0; i < 11; i++)
		mylist1.push_back(i);

	for (int i = 0; i < 10; i++)
		mylist2.push_back(i * 11);

	cout << '\n' << "**********************" << endl;
	for (list<int>::iterator it = mylist1.begin(); it != mylist1.end(); it++) {
		cout << ' ' << *it;
	}
	cout << '\n';
	for (list<int>::iterator it = mylist2.begin(); it != mylist2.end(); it++) {
		cout << ' ' << *it;
	}
	list<int>::iterator it = mylist1.begin();
	list<int>::iterator it2 = mylist2.begin();
	list<int>::iterator it3 = mylist2.end();
	it2++;
	it++;
	it2++;
	it3--;
	it3--;
	it3--;

	mylist2.splice(it, mylist1, it2, it3);
	cout << '\n' << "**********************" << endl;
	cout<<"before:"<<endl;
	for (list<int>::iterator it = mylist1.begin(); it != mylist1.end(); it++) {
		cout << ' ' << *it;
	}
	cout << '\n';
	for (list<int>::iterator it = mylist2.begin(); it != mylist2.end(); it++) {
		cout << ' ' << *it;
	}
	//mylist1.sort(parzyste);
	cout << '\n' << "**********************" << endl;
	cout<<"after:"<<endl;
	for (list<int>::iterator it = mylist1.begin(); it != mylist1.end(); it++) {
		cout << ' ' << *it;
	}
	cout << '\n';
	for (list<int>::iterator it = mylist2.begin(); it != mylist2.end(); it++) {
		cout << ' ' << *it;
	}

	return 0;
}
