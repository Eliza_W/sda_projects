//============================================================================
// Name        : MissionImpossible.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "KeyManager.h"
#include "Map.h"
#include "MapDisplayer.h"
#include "GameObject.h"
#include "GameLogic.h"
#include <string>
using namespace std;

void printHelp() {
	cout << "Help: " << endl << "o - exit" << endl << "r- idle" << endl
			<< "w - up" << endl << "s - down" << endl << "a - left" << endl
			<< "d - right" << endl << endl << endl;
}

int main() {

	bool exit = false;
	KeyManager *keyManager = new KeyManager; //utworzenie obiektu keyManager
	//utworzenie obiektu logiki
	Map *map = new Map;	//utworzenie obiektu mapy
	MapDisplayer *mapDisplayer = new MapDisplayer;//utworzenie obiektu mapDisplay
	mapDisplayer->setMap(map);	// przekazanie mapy do map displayera
	GameLogic *gameLogic = new GameLogic;
	map->setObjectList(gameLogic->getObjectList());
	gameLogic->setKeyManager(keyManager);

	gameLogic->configureGame();	//TODO sprawdzic konfiguracje
	//cout<<endl;
	map->generateMap();		//generowanie mapy
	mapDisplayer->displayMap();		//wyswietlanie mapy

	while (exit == false) { // petla glowna programu

		char key;
		cout << "Please enter key and press enter. (h for help)" << endl;

		cin >> key;
		if (key == 'h') {
			printHelp();
			continue;		//popros gracza o kolejny znak
		}
		keyManager->addNewKey(key);		// wczytamy kolejny znak
		if (gameLogic->nextTurn()) {
			map->generateMap();		//generowanie mapy
			mapDisplayer->displayMap();		//wyswietlanie mapy
			exit = true;
			cout << "GOTHA!!GAME OVER!!" << endl;
			continue;
		}
		map->generateMap();		//generowanie mapy
		mapDisplayer->displayMap();		//wyswietlanie mapy
		//logika gry

		//exit = true;
		//wczytamy kolejny znak
	}

	delete keyManager;		//usuniecie obiektu keyManager
	delete gameLogic;		//usuniecie obiektu logiki
	delete map;		//usuniecie obiektu mapy
	delete mapDisplayer;		//usuniecie obiektu mapDisplay
	cout << "Game ended succesfully" << endl;
	return 0;
}
