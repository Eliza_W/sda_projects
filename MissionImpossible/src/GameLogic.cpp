/*
 * GameLogic.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "GameLogic.h"
#include "Map.h"
#include <stdlib.h>
#include <time.h>

GameLogic::GameLogic() {
	// TODO Auto-generated constructor stub
	player = nullptr;
	for (int i = 0; i < 5; i++) {
		enemies[i] = nullptr;
	}
	srand(time(NULL));

}

GameLogic::~GameLogic() {
	// TODO Auto-generated destructor stub
	if (player != nullptr) {
		delete player;
	}
	for (int i = 0; i < 5; i++) {
		if (enemies[i] != nullptr) {
			delete enemies[i];
		}
	}
//	if(enemies!=nullptr){
//		delete[] enemies;
//	}
//	if(object_list !=nullptr){
//		delete[ ] object_list;
//	}
//}
}

void GameLogic::configureGame() {
	if (player != nullptr) {
		delete player;
	}
	for (int i = 0; i < 5; i++) {
		if (enemies[i] != nullptr) {
			delete enemies[i];
		}
	}

	player = new Player;
	object_list[0] = player;
	for (int i = 0; i < 5; i++) {
		enemies[i] = new Enemy;
		object_list[i + 1] = enemies[i];
	}
	for (int i = 0; i < 6; i++) { // przypisanie losowych pozycji playerowi i graczowi
		randomPosition(object_list[i]);
	}
	while(checkPlayerPosition()==false){
		randomPosition(player);
	}
}

bool GameLogic::nextTurn() {
	if(keyManager->getNextKey()=='o'){
		return true;
	}
	updatePlayerPosition();
	if(checkPlayerPosition()==false){
		return true;
	}

	//wygenerowanie nowej pozycji gracza
	//nowej pozycji przeciwnika
	//sprawdzenie czy jakikolwiek przeciwnik jest w odl jednego
	//gra zwraca true ->koniec
	return false;
}

void GameLogic::randomPosition(GameObject* object) {
	int x, y;
	bool success = false;

	while (success == false) {
		x = rand() % 25;	//wygenerujemy pozycje  %25 to zakres
		y = rand() % 25;

		//sprawdzimy pozycje
		if (checkPosition(x, y)) {
			object->setX(x);
			object->setY(y);
			success = true;
		}
	}
}

bool GameLogic::checkPosition(int x, int y) {
	for (int i = 0; i < 6; i++) {
				if (object_list[i]->getX() == x && object_list[i]->getY() == y) {
					return false;
				}
	}
	return true;	//pozycja jest pusta
}

bool GameLogic::checkPlayerPosition() {
	for (int i =1; i < 6; i++) {
		if (abs(object_list[i]->getX() - player->getX())<=1 && abs (object_list[i]->getY()- player->getY())<=1) {
			return false;
		}
	}
	return true;
}

void GameLogic::updatePlayerPosition() {
	switch (keyManager->getNextKey()) {
	case 'w':
		if (player->getY() - 1 >= 0) {
			player->setY(player->getY() - 1);
		}
		break;
	case 's':
		if (player->getY() + 1 < 25) {
			player->setY(player->getY() + 1);
		}
		break;
	case 'a':
		if (player->getX() - 1 >= 0) {
			player->setX(player->getX() - 1);
		}
		break;
	case 'd':
		if (player->getX() + 1 < 25) {
			player->setX(player->getX() + 1);
		}
		break;

	}
}
