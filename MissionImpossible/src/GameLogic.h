/*
 * GameLogic.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef GAMELOGIC_H_
#define GAMELOGIC_H_
#include "Player.h"
#include "Enemy.h"
#include "GameObject.h"
# include "KeyManager.h"

class GameLogic {
public:
	GameLogic();
	virtual ~GameLogic();
	bool nextTurn();//zwraca info na temat konca gry
	void configureGame();
	void randomPosition(GameObject *object);
	bool checkPosition(int x, int y);
	bool checkPlayerPosition();

	 GameObject** getObjectList()  {
		return object_list;
	}

	void setKeyManager( KeyManager*& keyManager) {
		this->keyManager = keyManager;
	}

private:
	void updatePlayerPosition();
	 KeyManager *keyManager;
	Player *player;
	Enemy *enemies[5];
	GameObject *object_list[6];// tutaj jest player i wszyscy enemy

};

#endif /* GAMELOGIC_H_ */
