//============================================================================
// Name        : Vectors.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

// constructing vectors
#include <iostream>
#include <vector>
using namespace std;

int main() {
//	// constructors used in the same order as described above:
//	std::vector<int> first;                              // empty vector of ints
//	std::vector<int> second(6, 88);                  // four ints with value 100
//	std::vector<int> third(second.begin(), second.end()); // iterating through second
//	std::vector<int> fourth(third);                       // a copy of third
//
//	// the iterator constructor can also be used to construct from arrays:
//	int myints[] = { 16, 2, 77, 29 };
//	std::vector<int> fifth(myints, myints + sizeof(myints) / sizeof(int)); //iterator na pierwszy, iterator na pierwszy plus wielkosc(w bajtach)/ wielkosc inta i to jest ilosc elementow
//
//	std::cout << "The contents of fifth are:";
//	for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
//		std::cout << ' ' << *it;
//	std::cout << '\n';
//	fifth.resize(3);
//	std::cout << "Now The contents of fifth are:";
//	for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
//		std::cout << ' ' << *it;
//	std::cout << '\n';
//	fifth.resize(8);
//	std::cout << "Now The contents of fifth are:";
//	for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
//		std::cout << ' ' << *it;
//	std::cout << '\n';
//	std::vector<int> Inty;
//	for (int i = 0; i < 100; i++)
//		Inty.push_back(i);
//
//	std::cout << "max size " << Inty.max_size()<<std::endl;
//
//	std::vector<double> Double;
//	for (int i = 0; i < 100; i++)
//		Double.push_back(i);
//
//	std::cout << "max size " << Double.max_size();

	int myInt;
	vector<int> Myvector;
	Myvector.reserve(20);

	cout << "My vectors stores capacity " << Myvector.capacity() << endl;

	cout << "My vectors stores size " << Myvector.size() << endl;
	cout << " Enter integers, 0 to end : " << endl;
	do {
		cin >> myInt;
		Myvector.push_back(myInt);
	} while (myInt);

	cout << "My vectors stores: " << Myvector.size() << endl;
	vector<int>::iterator it = Myvector.begin();
	for (int i = 0; i < Myvector.size(); i++, it++) {
		cout << Myvector[i] << " " << endl;  // mo�na wypisac tak :myVector[]
	}
	Myvector.pop_back();
//	Myvector.assign(3,555);
	cout << " After pop my vectors stores: " << Myvector.size() << endl;
	for (int i = 0; i < Myvector.size(); i++, it++) {
		cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
	}
	cout << "My vectors stores capacity " << Myvector.capacity() << endl;
	it = Myvector.begin();
	Myvector.insert(it, 3, 100000);  // iterator, ile sztuk, jaka liczba
	cout << " After insert my vectors stores: " << Myvector.size() << endl;
	for (int i = 0; i < Myvector.size(); i++) {
		cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
	}
	//vector<int> anothervector(2, 400);
	Myvector.insert(it + 2, 2,400); // wstawiamy na pozycje +2 czyli trzecia
	cout << "My vectors stores capacity " << Myvector.capacity() << endl;

	cout << " After insert  my vectors stores: " << Myvector.size() << endl;
	for (int i = 0; i < Myvector.size(); i++) {
		cout << Myvector.at(i) << " " << endl; // albo tak myVector.at(i) nie do konca rozumiem roznicy miedzy at a [i]
	}

	Myvector.erase(Myvector.begin() + 2);  // erase third element

	cout << " After insert and erase  my vectors stores: " << Myvector.size()
			<< endl;
	cout << "My vectors stores capacity " << Myvector.capacity() << endl;
	for (int i = 0; i < Myvector.size(); i++) {
		cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
	}
	Myvector.erase(Myvector.begin()+3,Myvector.begin() +6);  // erase  3 elements

	cout << " After insert and erase  my vectors stores: " << Myvector.size()
			<< endl;
	for (int i = 0; i < Myvector.size(); i++) {
		cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
	}
	Myvector.resize(3);
	cout << " After resize  my vectors stores: " << Myvector.size()
				<< endl;
		for (int i = 0; i < Myvector.size(); i++) {
			cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
		}
		Myvector.resize(19);
		cout << " After second resize  my vectors stores: " << Myvector.size()
					<< endl;
			for (int i = 0; i < Myvector.size(); i++) {
				cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
			}
			Myvector.resize(23);
					cout << " After second resize  my vectors stores: " << Myvector.size()
								<< endl;
						for (int i = 0; Myvector.empty(); i++) {
							cout << Myvector.at(i) << " " << endl;  // albo tak myVector.at(i)
						}

	Myvector.clear();
	cout << " After clear and erase  my vectors stores: " << Myvector.size()
				<< endl;
	cout << " After clear and erase  my vectors stores capacity: " << Myvector.capacity()
				<< endl;

	return 0;
}
