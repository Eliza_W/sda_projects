/*
 * B.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef B_H_
#define B_H_
#include <iostream>
#include "Y.h"
#include "Z.h"


#include "A.h"
using namespace std;
class B: virtual public A {
public:
	B();
	virtual ~B();
private:
	Y y;
	Z z;
};

#endif /* B_H_ */
