/*
 * A.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "A.h"
#include<iostream>
using namespace std;

A::A():
	number(0){
	cout<<" Constructing A..."<<endl;

}

A::~A(){
	cout<<" Destroing A"<<endl;
}
std::ostream& operator<<(std::ostream& nazwa_zmiennej, const A& a){
	nazwa_zmiennej<< " content of A: \n\t"<< a.number<<endl;
	return nazwa_zmiennej;
}
