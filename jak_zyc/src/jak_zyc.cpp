//============================================================================
// Name        : jak_zyc.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "A.h"
#include "B.h"
#include "C.h"
#include "D.h"
using namespace std;
void f( A& someA) {// jesli przez ref to nie ma kopii wiec pracuje na danym obiekcie i zmienia go.jesli nie ma ref to tworzy sie kopia i ta kopie zmienia
	cout<<someA.number<<endl;
	someA.number=5;
	cout<<someA.number<<endl;

}
int main() {
	cout << "!!!Hello World!!!" << endl;

	//A a; //zmienna tworzy si� na stosie
//	a.number=2;
	//f(a);
	//cout<<a;
//	cout<<a.number<<endl;
	B b;
	cout<<" more complicated: "<<endl;
	A *a = new D;
	delete a;

	cout << "!!!Exiting the program!!!" << endl;
	return 0;
}
