/*
 * MACHINE.h
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef MACHINE_H_
#define MACHINE_H_
#include <string>
#include<iostream>
//#include "StructCoffee.h"

using std::string;
const int max_size = 5;

struct Coffee {
	// lista produktow i cen
	string coffeename;
	float coffeeprice;
};

class MACHINE {
public:

	MACHINE();
	virtual ~MACHINE();
	Coffee list[max_size];
	void showList();
	void addProduct(string name, float price);
	void setClientsChoice();
	int getclientsChoice();
	float getSum();
	float returnChange();
	void giveCoffee();
	void showCoffee();
	string returnName(int a);
	float returnPrice(int b);
private:

	int number=8;
	float sum;
	float change;
	int size = 0;


};

#endif /* MACHINE_H_ */
