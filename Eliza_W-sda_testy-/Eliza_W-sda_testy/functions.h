/*
 * functions.h
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_
#include <string>
using namespace std;


//float anyPower(float a, float p);
double anyPower(float a, float p);

int secondPower(int a);

unsigned long long Silnia(int n);

int fibonacci(int n);
int NWD(int k, int n);
int daysInMonth(int month);
bool isDateValid(int day, int month, int year);

int digitsInNumber(long long number) ;

long long modulowanie(long long a) ;
int iloczynCyfr(long long number);
long long divide_ten(long long a);

string slownieLiczby(long long number);
int convertResult(int result);

#endif /* FUNCTIONS_H_ */
