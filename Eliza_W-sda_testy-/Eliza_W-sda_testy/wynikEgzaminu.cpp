/*
 * wynikEgzaminu.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include "functions.h"


TEST(TestingResultEgz, NoNeedToConvert1){
	EXPECT_EQ(41,convertResult(41));
}
TEST(TestingResultEgz, NoNeedToConvert2){
	EXPECT_EQ(31,convertResult(31));
}
TEST(TestingResultEgz, NeedToConvertTwo){
	EXPECT_EQ(60,convertResult(58));
}
TEST(TestingResultEgz, NeedToConvertOne){
	EXPECT_EQ(80,convertResult(79));
}
TEST(TestingResultEgz, InvalidData){
	EXPECT_EQ(0,convertResult(199));
}
TEST(TestingResultEgz, InvalidData2){
	EXPECT_EQ(0,convertResult(-100));
}
TEST(TestingResultEgz, Zero){
	EXPECT_EQ(0,convertResult(0));
}
TEST(TestingResultEgz, NoNeedToConvert3){
	EXPECT_EQ(66,convertResult(66));
}
