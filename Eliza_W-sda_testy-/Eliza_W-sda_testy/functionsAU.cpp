/*
 * functions.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */
#include <iostream>

//int secondPower(int number) {
//	return number * number;
//}
// TESTOWANIE PROGRAMOW AGATY
float powers(float number, int power) {
	float result = 1;

	if (power > 0)
		for (int i = 0; i < power; i++) {
			result = result * number;
		}

	else if (power < 0 && number != 0)
		number = 1 / number;
	power = (-1) * power;
	for (int i = 0; i < power; i++) {
		result = result * number;
	}

	return result;
}

long factorial(int number) {
	int i = 0;
	long result = 1;
	if (number == 0) {
		result = 1;
	} else
		for (i = number; i > 0; i--) {
			result = i * result;
		}
	return result;
}

int fib(int n) {

	if (n == 0)
		return 0;
	else if (n == 1)
		return 1;
	else if (n >= 2) {
		int c = fib(n - 1) + fib(n - 2);
		return c;
	} else
		return -1; //b�ad
}

int nwdd(int k, int n) {
	if (k == 0) {
		if (n > 0)
			return n;
		else
			return (-1) * n;
	}
	int c = nwdd(n % k, k);
	return c;
}

bool kalendarz(int dz, int m, int rok) {
	if (rok > 0 && m > 0 && dz > 0) {
		if ((rok % 4 == 0 && rok % 100 != 0) || rok % 400 == 0) // rok przestepny
				{
			if (((m == 1 || m == 3 || m == 5 || m == 7 || m == 9 || m == 11)
					&& dz <= 31)
					|| ((m == 4 || m == 6 || m == 8 || m == 10 || m == 12)
							&& dz <= 30) || (m == 2 && dz <= 29)) {
				return 1;
			} else
				return 0;
		} else if (((m == 1 || m == 3 || m == 5 || m == 7 || m == 9 || m == 11)
				&& dz <= 31)
				|| ((m == 4 || m == 6 || m == 8 || m == 10 || m == 12)
						&& dz <= 30) || (m == 2 && dz <= 28)) {
			return 1;
		} else
			return 0;
	} else
		return 0;
}
