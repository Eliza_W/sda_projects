/*
 * funktions.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#include <cmath>
#include <string>
using namespace std;
int secondPower(int a) {
	return a * a;
}
//float anyPower(float a, float b) {
//	float wynik = 1;
//	if (b == 0)
//		return 1;      //gdy b=0
//	if (b > 0)          //gdy b>0
//			{
//		while (b--) {
//			wynik *= a;
//		}
//		return wynik;
//	} else                //gdy b<0
//	{
//		while (b++) {
//			wynik *= a;
//		}
//		return 1 / wynik;
//	}
//}
double anyPower(float a, float p) {
	double wynik = 1;
	if (p > 0) {
		for (int i = 0; i < p; i++) {
			wynik = wynik * a;
		}

	}

	else if (p < 0) {
		while (p < 0) {
			wynik = wynik * a;
			p = p + 1;

		}
		wynik = 1 / wynik;

	}
	return wynik;
}
unsigned long long Silnia(int n) {
	unsigned long long a = 1; //mnozymy dwie poprzednie, w petli jak "poprzednia" a bedzie iloczyn dwoch poprzednich. a i b na poczatku maja wartosc 2 pierwszych liczb
	unsigned long long b = 2;

	unsigned long long silnia = 1;

	if (n == 0) {
		return 1;
	}

	else if (n > 0) {

		for (int i = 1; i < n; i++) {
			silnia = a * b;
			b = b + 1;
			a = silnia;
		}
		return silnia;
	}

	return 0;
}
int fibonacci(int n) {
//	Ka�da liczba w ci�gu jest sum� dw�ch poprzednich (poza pierwsz� i drug�).

	int m, f, fn;
	f = 0;
	fn = 1;

	if (n == 1) {
		return 1;
	} else if (n == 0) {
		return 0;
	} else if (n > 0) {

		for (int i = 2; i < n; ++i) //i=2, bo pierwsza i druga pozycje juz mamy
				{
			m = f + fn;
			f = fn;
			fn = m;

		}
		return fn;
	}

	return -1;
}
int NWD(int k, int n) {
	if (k == 0) {
		return n;
	} else if (n == 0) {
		return k;
	} else if (k > 0 && n > 0) {
		int nwd = NWD(n % k, k);
		return nwd;
	} else
		return 0;
}

int daysInMonth(int month) {

	while (month > 0) {

		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
			break;

		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
			break;
		case 2:
			return 28;
		default:
			return 0;

		}

	}
	return 0;
}

bool isDateValid(int day, int month, int year) {

	if (year > 0 && day > 0 && day <= 31 && month > 0 && month <= 12) {

		switch (month) {
		case 1:

		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (day <= 31) {
				return 1;
			}
			break;

		case 2: {

			if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
				if (day <= 29) {
					return 1;
				}
			}

			else {
				if (day <= 28) {

					return 1;
				} // else {
				  //return 0;
				  //}
			}
			break;

			case 4:

			case 6:
			case 9:
			case 11:
			if (day <= 30) {
				return 1;
			}
			break;

			default:
			return 0;

		}

		}

	}
	return 0;
}

int digitsInNumber(long long number) {
	int counter = 1;
	while (number > 9 || number < -9) {
		number = number / 10;

		++counter;

	}
	return counter;
}

long long modulowanie(long long a) {

	return a % 10;
}
long long divide_ten(long long a) {

	return a / 10;
}
int iloczynCyfr(long long number) {

	int iloczyn = 1;
	int cyfra;
	if (number < 0) {
		number = number * (-1); // na wypadek liczby ujemnej
	}

	if (number != 0) {

		while (number > 0) {

			cyfra = modulowanie(number);

			number = divide_ten(number);

			iloczyn = iloczyn * cyfra; //iloczyn ustawiony na 1, bo na poczatku jest tylko jedna cyfra

		}

		return iloczyn;
	}
	return 0;
}

string slownieLiczby(long long number) // nie skonczylam jeszcze!
		{
	int zeroNumber = 0;
	long long odwrocona_liczba = 0;
	int odwrocona_cyfra;
	int cyfra;
	if (number == 0) {
		return "zero";
	}
	string slownie = "";
	if (number < 0) {
		number = number * (-1); // gdyby liczba byla ujemna
		slownie = "minus ";
	}

	if (number % 10 == 0) {

		long long temp_number = number;

		while (temp_number % 10 == 0) {
			temp_number = temp_number / 10;
			zeroNumber++;
		}
//		for(int i = 0; i<zeroNumber;i++)
//		slownie= slownie + "zero ";
	}

	while (number > 0) {

		odwrocona_cyfra = number % 10;
		number = number / 10;
		odwrocona_liczba = 10 * odwrocona_liczba + odwrocona_cyfra; //odwracam liczbe
	}

	while (odwrocona_liczba > 0) {
		cyfra = odwrocona_liczba % 10;
		odwrocona_liczba = odwrocona_liczba / 10; //wyczytuje od tylu odwrocona liczbe.

		switch (cyfra) {
		case 0:
			slownie = slownie + "zero ";
			break;
		case 1:
			slownie = slownie + "jeden ";
			break;
		case 2:
			slownie = slownie + "dwa ";
			break;
		case 3:
			slownie = slownie + "trzy ";
			break;
		case 4:
			slownie = slownie + "cztery ";
			break;
		case 5:
			slownie = slownie + "piec ";
			break;
		case 6:
			slownie = slownie + "szesc ";
			break;
		case 7:
			slownie = slownie + "siedem ";
			break;
		case 8:
			slownie = slownie + "osiem ";
			break;
		case 9:
			slownie = slownie + "dziewiec ";
			break;
		}

	}
	for (int i = 0; i < zeroNumber; i++)// dodaje zera. troche kiepskie, ale dziala :P)
		slownie = slownie + "zero ";
	return slownie;

}

int convertResult(int result) {

	while (result > 0 && result < 100) {
		if (result > 40) {
			if (((result + 2) % 5 == 0)) {
				return result + 2;
			} else if ((result + 1) % 5 == 0) {
				return result + 1;
			}
		}
		return result;
	}
	return 0;
}
