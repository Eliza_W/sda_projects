/*
 * Interface.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef INTERFACE_H_
#define INTERFACE_H_
#include "LectureRoom.h"
#include "Kurs.h"
#include "Student.h"
#include "Teacher.h"
#include "Data.h"
#include <iostream>
#include <string>
using namespace std;

class Interface {
public:
	Interface(int count=0);
	virtual ~Interface();
	string firstname;
	string lastname;
	int kurs;
	int counter;


	void Dane(Student *Student01);
};

#endif /* INTERFACE_H_ */
