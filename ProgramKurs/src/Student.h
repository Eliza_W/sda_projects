/*
 * Student.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef STUDENT_H_
#define STUDENT_H_
#define MAX_NR_OF_KURS_FOR_ST 3

#include <string>

#include <iostream>
using namespace std;
class Kurs;
class Student {
public:
	string firstname;
	string lastname;
	int age;
	string email;
	string course_name;
	Kurs *kursy[MAX_NR_OF_KURS_FOR_ST];

	int ile_kursow=0;



	Student();
	virtual ~Student();
	void ShowStudent();
	void AddCourse(Kurs *n);
	void show_kursy();



};

#endif /* STUDENT_H_ */
