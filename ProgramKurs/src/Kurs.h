/*
 * Kurs.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#include "LectureRoom.h"
#include "Student.h"
#include "Data.h"

#ifndef KURS_H_
#define KURS_H_
using namespace std;
#define MAX_NR_OF_TEACHERS_PER_COURSE   10
#define MAX_NR_OF_STUDENTS 3


class Kurs {
public:
	string course_name;
	string date_start;
	string date_finish;
	LectureRoom *room;

	string description;
	int max_nr_of_students;
	int nr_of_free_places;
	Student *participants[MAX_NR_OF_STUDENTS];






	void WypiszStudenta();

	Kurs(string cn, int mnos) ;
	virtual ~Kurs();
	void AddStudent(Student *new_participant);
	void WypiszKurs();
	void AddRoom(LectureRoom *sala);
	void ShowCourse();
	void wypiszData(Data jakasdata);


//	void ShowStudent();
//	void ShowTeacher();
};

#endif /* KURS_H_ */
