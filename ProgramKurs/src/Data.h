/*
 * Data.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef DATA_H_
#define DATA_H_

class Data {
public:
	Data(int d,int m,int y);
	virtual ~Data();
	int day;
	int month;
	int year;
	void startDate();
	void finishDate();
	int getDay();
	int getMonth();
};

#endif /* DATA_H_ */
