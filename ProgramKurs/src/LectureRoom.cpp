/*
 * LectureRoom.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "LectureRoom.h"
#include "Kurs.h"
#include <iostream>
using namespace std;
LectureRoom::LectureRoom(int number, int size, char adress, int ile_kurs) {
	// TODO Auto-generated constructor stub
	room_number = number;
	room_size = size;
	building = adress;
	ile_kursow = ile_kurs;
}
void LectureRoom::Description() {
	cout << " Sala nr " << room_number << " w budynku " << building << " na "
			<< room_size << " miejsc." << endl;
}
void LectureRoom::AddCourse(Kurs *nazwa_kursu) {
	if (ile_kursow < MAX_NR_OF_COURSE_PER_ROOM) {
		course[ile_kursow] = nazwa_kursu;
		ile_kursow++;
	}
}
void LectureRoom::ShowCourse() {
	for (int i = 0; i < ile_kursow; i++) {
		course[i]->WypiszKurs();
		cout<<" ";

	}
}
