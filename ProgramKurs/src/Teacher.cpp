/*
 * Teacher.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Teacher.h"
#include "Kurs.h"
#include<string>
#include<iostream>

Teacher::Teacher(string fname, string lname, int wiek, string mail,int ile_kurs) {
	// TODO Auto-generated constructor stub
	firstname = fname;
	lastname = lname;
	email = mail;
	age = wiek;
	ile_kursow = ile_kurs;
}

void Teacher::Description() {
	cout << firstname << " " << lastname << ", lat " << age << ". Email: "
			<< email << endl;
}
void Teacher::AddCourse(Kurs *nazwa_kursu) {
	if (ile_kursow < MAX_NR_OF_COURSE_FOR_TEACHER) {
		course[ile_kursow] = nazwa_kursu;
		ile_kursow++;
	}
}
void Teacher::ShowCourse() {
	for (int i = 0; i < ile_kursow; i++) {
		course[i]->WypiszKurs();
		cout << " ";
	}
	cout<<endl;
}
