/*
 * LectureRoom.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LECTUREROOM_H_
#define LECTUREROOM_H_

#include <string>
#define MAX_NR_OF_COURSE_PER_ROOM 3
using namespace std;

class Kurs;

class LectureRoom {
		int room_number;
		int room_size;
		char building;
		int ile_kursow;
		string course_name;



public:
		Kurs *course[MAX_NR_OF_COURSE_PER_ROOM];
	LectureRoom(int number, int size, char adress, int ile_kurs=0);
	void Description();
	void AddCourse(Kurs *nazwa_kursu);
	void ShowCourse();
};

#endif /* LECTUREROOM_H_ */
