/*
 * Teacher.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef TEACHER_H_
#define TEACHER_H_
#include "Kurs.h"
#include <iostream>
#include <string>
#define MAX_NR_OF_COURSE_FOR_TEACHER 2
using namespace std;
class Teacher {
	string firstname;
	string lastname;
	string email;
	int age;
	int ile_kursow;
	Kurs *course[MAX_NR_OF_COURSE_FOR_TEACHER];
public:

	Teacher(string fname, string lname, int wiek,string mail,  int ile_kurs=0);
	void Description();
	void AddCourse(Kurs *nazwa_kursu);
	void ShowCourse();

};

#endif /* TEACHER_H_ */
