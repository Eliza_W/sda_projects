//============================================================================
// Name        : rzutowanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Event.h"
#include "OpenFileEvent.h"
using namespace std;

int main() {
	Event gev = Event();
	OpenFileEvent oev("a.txt");

	Event *ev = &gev;
	cout<<ev->getEventType()<<"\n";
	ev = &oev;
	cout<<ev->getEventType()<<"\n";



//	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
//	float aNumber=8.55;
//	int someOtherNumber = static_cast<int>(aNumber);
//	int someOtherNumber2 = aNumber+5.7;
//	int someOtherNumber3 = int(aNumber);
//	cout<<someOtherNumber<<" to jest ten numer"<<endl;
//	cout<<someOtherNumber2<<" to jest ten numer"<<endl;
//	cout<<someOtherNumber3<<" to jest ten numer"<<endl;
	return 0;
}
