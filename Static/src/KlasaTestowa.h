/*
 * KlasaTestowa.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef KLASATESTOWA_H_
#define KLASATESTOWA_H_

class KlasaTestowa {
public:
	KlasaTestowa();
	KlasaTestowa(const KlasaTestowa&);
	virtual ~KlasaTestowa();

	static int  getR()  {
		return r;
	}

	void setR( int rm) {
		r = rm;
	}



private:
	static int r;
};

#endif /* KLASATESTOWA_H_ */
